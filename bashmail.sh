#!/bin/bash

# config file which is used when config file is not specifed by program arguments
default_config_file="config.sh"


# help to run in help mode
function help {
	echo "This is help for BASHMAIL"
	
	echo " "
	
	echo "About:"
	echo "This program could check:"
	echo " - total ram size"
	echo " - number or system processors"
	echo " - all IP addresses (need hostname)"
	echo " - status of all mdraid arrays (need mdadm and privileges)"
	echo " - smart of all drives (need smartmontools and privileges)"
	echo "If program is executed first time or any checked value changed emaill will be sent:"
	echo " - multiple recipients are supported"
	echo " - no encrypted communication with SMTP server requires nc(netcat)"
	echo " - encrypted communication with SMTP server requires openssl"
	 
	echo " "
	
	echo "Usage:"
	echo "bashmail.sh help - show this massage"
	echo "bashmail.sh [configfile] - run program and send email if values changed or program is executed fiest time"
	echo "bashmaul.sh testmail [configfile] - send test email with values like default values from configfile"
	
	echo " "
	
	echo "Config file:"
	echo "Default config file is: config.sh"
	echo "Before first usage you must copy example_config.sh and edit it"
	echo "This file store SMTP, default mail parameteres and enabled checking parameters configuration"
}

# display wrong usage message
function wrong_usage {
	echo "Wrong program usage" 1>&2
	echo "Try: bashmail.sh help" 1>&2 
	exit 1
}

# send email
function e_mail {
	# tls enctyption is shourtcat fot tls1_2 
	if [ $smtp_encryption = "tls" ]
	then 
		smtp_encryption="tls1_2"
	fi
	
	
	# base64 encoding of smtp_username and smtp_password
	smtp_user_encoded=`echo -nE $smtp_user | base64`
	smtp_password_encoded=`echo -nE $smtp_password | base64`
	
	
	# this function generetes smtp_output commands for smtp smtp_server
	# commands are delay to allow of communication with smtp_server with problems with pipeline mode ( e.g. smtp.gmail.com )
	function tx {
		sleep $smtp_command_delay
		
		# EHLO
		echo 'EHLO '$smtp_server | tee $smtp_output
		sleep $smtp_command_delay
		
		# AUTH LOGIN
		echo 'AUTH LOGIN' | tee $smtp_output
		sleep $smtp_command_delay
		
		# smtp_user
		echo $smtp_user_encoded | tee $smtp_output
		sleep $smtp_command_delay
		
		# smtp_password
		echo smtp_password >> $smtp_output
		echo $smtp_password_encoded
		sleep $smtp_command_delay
		
		# MAIL FROM
		echo "MAIL FROM:<"$smtp_user">" | tee $smtp_output
		sleep $smtp_command_delay
		
		# RCPT TO for all mail_recipients
		for recipient in `echo $mail_recipients | tr ',' '\n'`
		do
			echo "RCPT TO:<"$recipient">" | tee $smtp_output
			sleep $smtp_command_delay
		done
		
		# DATA
		echo "DATA" | tee $smtp_output
		sleep $smtp_command_delay
		
		# From: sender name
		echo "From: "$smtp_user | tee $smtp_output
		sleep $smtp_command_delay
		
		# mail_subject
		echo "Subject: "$mail_subject | tee $smtp_output
		sleep $smtp_command_delay
		
		# To: mail_recipients names
		echo "To: "$mail_recipients | tee $smtp_output
		sleep $smtp_command_delay
		
		# Mime-Version
		echo "Mime-Version: 1.0;" | tee $smtp_output
		sleep $smtp_command_delay
		
		# mail_content-mail_type text or html and mail_charset
		echo "Content-Type: text/"$mail_type"; mail_charset="$mail_charset";" | tee $smtp_output
		sleep $smtp_command_delay
		
		# mail_content
		echo -e "$mail_content" | tee $smtp_output
		sleep $smtp_command_delay
		
		
		# mail_content end
		echo "." | tee $smtp_output
		sleep $smtp_command_delay
		
		# QUIT
		echo "QUIT" | tee $smtp_output
		sleep $smtp_command_delay
	}
	
	
	# select proper progarm and params to establish communication with smtp smtp_server based on smtp_encryption mail_type
	# commands are smtp_timeout to protect against e.g. smtp syntex error
	if [ $smtp_encryption = "none" ]
	then 
		function send_command {
			tx | timeout $smtp_timeout nc -v -C $smtp_server $smtp_port | tee $smtp_output
		}
	elif [ $smtp_encryption = "starttls" ]
	then
		function send_command {
			tx | timeout $smtp_timeout openssl s_client -connect $smtp_server:$smtp_port -crlf -starttls smtp -ign_eof | tee $smtp_output
		}
	elif [ $smtp_encryption = "tls1" ] || [ $smtp_encryption = "tls1_1" ] || [ $smtp_encryption = "tls1_2" ] || [ $smtp_encryption = "tls1_3" ] || [ $smtp_encryption = "dtls" ]
	then
		function send_command {
			tx | timeout $smtp_timeout openssl s_client -connect $smtp_server:$smtp_port -crlf -$smtp_encryption -ign_eof | tee $smtp_output
		}
	else
		echo "Unkown smtp smtp_encryption mail_type: $smtp_encryption" 1>&2
		return 1
	fi
	
	# try email sending a few times
	try=1
	while [ $try -le $smtp_retry ]
	do
		result=`send_command`
		if [[ "$result" == *"250 2.0.0"* ]]; then
			echo "EMAIL SENT"
			return 0
		fi
		
		try=$[try + 1]
		echo "ERROR OCCURED!!!" 1>&2
		echo "TRYING $try/$smtp_retry time" 1>&2
	done
	
	
	return 1
}

# send mail with default parameters from conifg_gile
function testmail {
	echo "Sending test mail"
	e_mail
	exit_code=$?
	exit $exit_code
}

function bashmail {
	nl="\n"
	report=""
	
	if [ $check_ip = "Yes" ]
	then
		report=$report"------ IP CHECK ------"$nl
		report=$report`hostname -I`
		report=$report$nl
	fi
	
	if [ $check_ram = "Yes" ]
	then
		report=$report"------ RAM CHECK ------"$nl
		report=$report`free | head -n 2 | tail -n 1 | awk '{print $2}'`
		report=$report"="
		report=$report`free -h | head -n 2 | tail -n 1 | awk '{print $2}'`
		report=$report"="
		report=$report`free -h --si | head -n 2 | tail -n 1 | awk '{print $2}'`
		report=$report$nl
	fi
	
	
	
	if [ $check_processors_count = "Yes" ]
	then
		report=$report"------ PROCESSORS COUNTER CHECK ------"$nl
		report=$report`cat /proc/cpuinfo | grep "model name" | wc -l`
		report=$report$nl
	fi
	
	if [ $check_mdstat = "Yes" ]
	then
		report=$report"------ MDRAID STATUS CHECK ------"$nl
		report=$report`cat /proc/mdstat`
		report=$report$nl
	fi
	
	if [ $check_smart = "Yes" ]
	then
		report=$report"------ SMART CHECK ------"$nl
		for dev in  `smartctl --scan | awk '{print $1}'`
		do
			report=$report$dev$nl
			report=$report`smartctl "$dev" -i | grep "Model Number"`$nl
			report=$report`smartctl "$dev" -i | grep "Serial Number"`$nl
			report=$report`smartctl "$dev" -H | grep result`$nl
		done
		report=$report$nl
	fi
	
	report=`echo -ne "$report"`
	echo "$report"
	echo "------ ---------- ------"
	echo " "
	
	send_reason=""

	# loog for reason to send email (first run or values changed)
	if [ -e $cache_file ]
	then
		
		old_cache=`cat $cache_file`
		
		if [ ! "$old_cache" = "$report" ]
		then
			echo "Something chaned"
			send_reason="Value changed"
		else
			echo "Nothing change"
		fi
			
	else
		echo "This is first time bashmail was run"
		send_reason="First run"
	fi

	# email will be send if any reason is set
	if [ ! "$send_reason" = "" ]
	then
		mail_subject="BASHMAIL on "`hostname`": "$send_reason
		mail_content=$report
		
		e_mail
		e_mail_exit_code=$?
		
		if [ $e_mail_exit_code != 0 ]
		then
			exit $e_mail_exit_code
		fi
		
		echo "$report" > $cache_file
	fi

	exit 0
}

if ! command -v nc &> /dev/null
then
    echo "Warning: nc(netcat) is unavailable"  1>&2 
fi


if ! command -v openssl &> /dev/null
then
    echo "Warning: openssl is unavailable"  1>&2 
fi

# program argumets process according to usage rules
# detect mode and config_file to use
if [ $# = 0 ]
then
	mode="bashmail"
	config_file=$default_config_file
elif [ $# = 1 ]
then
	if [ $1 = "help" ] || [ $1 = "testmail" ]
	then
		mode=$1
		config_file=$default_config_file
	else
		config_file=$1
	fi
elif [ $# = 2 ]
then
	if [ $1 = "testmail" ]
	then
		mode=$1
		config_file=$2
	else
		wrong_usage
	fi
else
	wrong_usage
fi


# run help if detected mode is help
if [ $mode = "help" ]
then
	help
	exit 0
fi


# load config file if program mode is bashmail or testmail
if [ ! -f $config_file ]
then
	echo "Could not locate config file: $config_file" 1>&2
	echo "Try: bashmail.sh help" 1>&2 
	exit 1
fi
source $config_file


# run function for active mode
if [ $mode = "testmail" ]
then 
	testmail
elif [ $mode = "bashmail" ]
then
	bashmail
else
	echo "Unsupported mode" 1>&2
	echo "Try: bashmail.sh help" 1>&2 
	exit 1
fi
