# cachefile path - diffrent bashmail instances use diffrent cashe files
cache_file="/tmp/bashmail.cache"

# enable checking of each parameter
check_ram=Yes
check_ip=Yes
check_processors_count=Yes
check_mdstat=No # need mdadm and privileges
check_smart=No # need smartctl and privileges

# SMTP server address 
smtp_server=smtp.example.com

# SMTP server port
smtp_port=25

# SMTP encryption mode
# none/tls/tls1/tls1_1/tls1_2/tls1_3/dtls/starttls
# tls is shortcut for tls1_2
# none option requires netcat
# all other encryption options requires openssl
smtp_encryption=none

# SMTP user name
smtp_user=user@example.com

# SMTP user password
smtp_password=password

# delay between sending commands to SMTP server
# some server works in pipeline mode and this value could be set to 0
# some server needs delay e.g. smtp.gmail.com
smtp_command_delay=2

# timeout for communication with SMTP server
smtp_timeout=60

# how many times program will try to email if sending fauld
smtp_retry=3

# output device for displaing communication with SMTP server
smtp_output="/dev/tty"

# mail subject and content encoding
mailcharset="UTF-8"

# default mail recipients separeted by commas
mail_recipients=recipient1@example.com,recipient2@example.com

# default mail subject
mail_subject="Test mail subject"

# default mail content
mail_content="This is\nbashmail\ntest mail content"

# default mail type
# text/html
mail_type=text

