# bash-mail

This is help for BASHMAIL
 
---

About:

This program could check:
 - total ram size
 - number or system processors
 - all IP addresses (need hostname)
 - status of all mdraid arrays (need mdadm and privileges)
 - smart of all drives (need smartmontools and privileges)

If program is executed first time or any checked value changed emaill will be sent:
 - multiple recipients are supported
 - no encrypted communication with SMTP server requires nc(netcat)
 - encrypted communication with SMTP server requires openssl
 
---

Usage:

`bashmail.sh help` - show this massage

`bashmail.sh [configfile]` - run program and send email if values changed or program is executed fiest time

`bashmaul.sh testmail [configfile]` - send test email with values like default values from configfile

 ---

Config file:

Default config file is: config.sh

Before first usage you must copy example_config.sh and edit it

This file store SMTP, default mail parameteres and enabled checking parameteres configuration
